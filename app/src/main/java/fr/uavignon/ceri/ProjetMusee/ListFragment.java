package fr.uavignon.ceri.ProjetMusee;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.Item;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;

    private RecyclerAdapter adapter;
    private ProgressBar progress;
    private EditText searchField;
    private TextView foundCount;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {

        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        progress = getView().findViewById(R.id.progressList);
        adapter.setListViewModel(viewModel);
        searchField = getView().findViewById(R.id.editSearchArg);
        foundCount = getView().findViewById(R.id.foundCount);
    }

    public void search(String arg) {

        arg = arg.toUpperCase();
        ArrayList<Item> itemArrayList = new ArrayList<>();

        if (adapter.getItemList()!=null) {
            for (Item item : adapter.getItemListFull()) {
                if (item.getName().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getBrand()!=null && item.getBrand().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getCategories()!=null && item.getCategories().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getDescription().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getTechnicalDetails()!=null && item.getTechnicalDetails().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getId().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getYear()>0 && String.valueOf(item.getYear()).toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
                else if (item.getTimeFrame()!=null && item.getTimeFrame().toUpperCase().contains(arg)){
                    itemArrayList.add(item);
                }
            }
            adapter.updateItemList(itemArrayList);
        }
        else {
            adapter.updateItemList(adapter.getItemListFull());
        }
        foundCount.setText(String.valueOf(adapter.getItemCount()));
    }

    private void observerSetup() {

        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> adapter.setItemList(items));

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
            );

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}