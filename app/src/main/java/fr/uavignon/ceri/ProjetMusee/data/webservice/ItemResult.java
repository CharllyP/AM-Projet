package fr.uavignon.ceri.ProjetMusee.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.Item;

public class ItemResult {

    public static void transferInfo(ItemResponse itemResponse, String key, ArrayList<Item> items) {

        Item item = new Item();
        item.setId(key);
        item.setName(itemResponse.name);
        item.setDescription(itemResponse.description);
        item.setWorking(itemResponse.working);
        if(itemResponse.year != null) {
            item.setYear(itemResponse.year);
        }
        if(itemResponse.brand != null) {
            item.setBrand(itemResponse.brand);
        }
        item.setImage("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + key + "/thumbnail");
        if(itemResponse.categories != null) {
            String categories = itemResponse.categories.get(0);
            for (int i=1;i<itemResponse.categories.size();i++){
                categories = categories+"; "+itemResponse.categories.get(i);
            }
            item.setCategories(categories);
        }
        if(itemResponse.timeFrame != null) {
            String tf = String.valueOf(itemResponse.timeFrame.get(0));
            for (int i=1;i<itemResponse.timeFrame.size();i++){
                tf = tf+" - "+itemResponse.timeFrame.get(i);
            }
            item.setTimeFrame(tf);
        }
        if(itemResponse.technicalDetails != null) {
            String technicalDetails = "";
            for (int i=0;i<itemResponse.technicalDetails.size();i++){
                technicalDetails = technicalDetails+"-"+itemResponse.technicalDetails.get(i);
                if(i<itemResponse.technicalDetails.size()-1) {
                    technicalDetails += "\n";
                }
            }
            item.setTechnicalDetails(technicalDetails);
        }

        items.add(item);
    }
}
