package fr.uavignon.ceri.ProjetMusee;


import android.app.Activity;
import android.content.Context;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import fr.uavignon.ceri.ProjetMusee.data.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.ProjetMusee.RecyclerAdapter.ViewHolder> {

    private static int cpt = 1;
    private ArrayList<Item> itemList;
    private ArrayList<Item> itemListFull;
    private ListViewModel listViewModel;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;

        if(cpt%2 == 0) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }
        else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout2, viewGroup, false);
        }

        cpt++;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.itemTitle.setText(itemList.get(i).getName());

        if(itemList.get(i).getImage() != null) {
            Glide.with(viewHolder.itemView.getContext())
                    .load(itemList.get(i).getImage())
                    .into(viewHolder.imgItem);
        }

        viewHolder.itemCategories.setText(itemList.get(i).getCategories());

        String year = String.valueOf(itemList.get(i).getYear());
        if(Integer.parseInt(year) <= 0) {
            year = "Année inconnue";
        }
        viewHolder.itemYear.setText(year);

        viewHolder.itemBrand.setText(itemList.get(i).getBrand());
    }

    @Override
    public int getItemCount() {

        if(itemList != null) {
            return  itemList.size();
        }
        else {
            return 0;
        }
    }

    public void setItemList(ArrayList<Item> items) {

        itemList = items;
        itemListFull = items;
        notifyDataSetChanged();
    }

    public ArrayList<Item> getItemList() {
        return itemList;
    }

    public void updateItemList(ArrayList<Item> list){
        itemList = list;
        notifyDataSetChanged();
    }

    public ArrayList<Item> getItemListFull() {
        return itemListFull;
    }

    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTitle;
        TextView itemYear;
        ImageView imgItem;
        TextView itemCategories;
        TextView itemBrand;

        ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemYear = itemView.findViewById(R.id.item_year);
            imgItem = itemView.findViewById(R.id.item_image);
            itemCategories = itemView.findViewById(R.id.item_categories);
            itemBrand = itemView.findViewById(R.id.item_brand);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(v -> {

                String id = RecyclerAdapter.this.itemList.get(getAdapterPosition()).getId();
                ListFragmentDirections  .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                action.setItemNum(id);
                Navigation.findNavController(v).navigate(action);

            });

            itemView.setOnLongClickListener(v -> {

                idSelectedLongClick = RecyclerAdapter.this.itemList.get(getAdapterPosition()).getId();
                if (actionMode != null) {
                    return false;
                }

                Context context = v.getContext();
                actionMode = ((Activity)context).startActionMode(actionModeCallback);
                v.setSelected(true);

                return true;
            });
        }
     }
}