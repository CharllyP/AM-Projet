package fr.uavignon.ceri.ProjetMusee.data.webservice;

import java.util.List;
import java.util.Map;

public class ItemResponse {
    public Boolean working = false;
    public String description = null;
    public String name = null;
    public Integer year = null;
    public String brand = null;
    //public Map<String, String> image = null;
    public List<String> categories = null;
    public List<Integer> timeFrame = null;
    public List<String> technicalDetails = null;
}
